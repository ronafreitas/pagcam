import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { NgxGalleryModule } from 'ngx-gallery';
//import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
//import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { UserService } from './services/user.service';
import { LoginService } from './services/login.service';
import { PublicationService } from './services/publication.service';
import { FollowerService } from './services/follower.service';
import { FileService } from './services/file.service';
import { LikeService } from './services/like.service';
import { CommentService } from './services/comment.service';
import { ChatService } from './services/chat.service';
import { ChatlistService } from './services/chatlist.service';
import { SocketService } from './services/socket.service';
import { PhotoService } from './services/photo.service';
import { GroupService } from './services/group.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { PublishComponent } from './pages/publish/publish.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { FollowersComponent } from './pages/followers/followers.component';
import { FollowingComponent } from './pages/following/following.component';
import { ExtractComponent } from './pages/extract/extract.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { CreditsComponent } from './pages/credits/credits.component';
import { CallingComponent } from './pages/calling/calling.component';
import { UserComponent } from './pages/user/user.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { MomentsComponent } from './components/moments/moments.component';
import { MenuComponent } from './components/menu/menu.component';
import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';
import { SearchComponent } from './pages/search/search.component';
import { GroupComponent } from './pages/group/group.component';
import { CreateComponent } from './pages/group/create/create.component';
import { EditComponent } from './pages/group/edit/edit.component';
import { ListComponent } from './pages/group/list/list.component';
import { ShowComponent } from './pages/group/show/show.component';
import { PublicationComponent } from './pages/group/publication/publication.component';
import { FeedComponent } from './components/feed/feed.component';
import { FilterUserPipeModule } from './pipes/filteruser.pipe';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    PublishComponent,
    MessagesComponent,
    FollowersComponent,
    FollowingComponent,
    ExtractComponent,
    SettingsComponent,
    CreditsComponent,
    CallingComponent,
    UserComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    MomentsComponent,
    MenuComponent,
    EditProfileComponent,
    SearchComponent,
    GroupComponent,
    CreateComponent,
    EditComponent,
    ListComponent,
    ShowComponent,
    PublicationComponent,
    FeedComponent,
    FilterUserPipeModule
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    TabsModule.forRoot(),
    Ng2ImgMaxModule,
    NgxGalleryModule,
    //BsDropdownModule.forRoot(),
    //TooltipModule.forRoot(),
  ],
  providers: [
    UserService,
    LoginService,
    PublicationService,
    FollowerService,
    FileService,
    LikeService,
    CommentService,
    ChatService,
    ChatlistService,
    PhotoService,
    GroupService,
    SocketService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
