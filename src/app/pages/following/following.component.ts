import { Component, OnInit } from '@angular/core';
import { FollowerService } from '../../services/follower.service';
import { FilterUserPipeModule } from '../../pipes/filteruser.pipe';
@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
	styleUrls: ['./following.component.css'],
	providers: [ FilterUserPipeModule ]
})
export class FollowingComponent implements OnInit {

	public userinsession: any = JSON.parse(localStorage.getItem('user'));
	public followingUser: Array<any> = [];
	public followingTotal: number=0;

	constructor(private followerService: FollowerService,private flp:FilterUserPipeModule){}

	ngOnInit(){
		this.GetFollowerByUserId();
	}

	// estou seguindo os 'USERs'
	// mudar nome do método, ta invertido 'followers' <=> 'following'
	GetFollowerByUserId(){
		this.followerService.GetFollowerByUserId(this.userinsession._id).subscribe(
		  res => {
		    this.followingTotal = res.total;
		    this.followingUser = res.response.map(item => {
		      return item.followerId
		    })
		  }, err => {
		    console.log(err)
		  })
	}
	filterUser(event:any){
    var evn = event.target.value;
    var nome = evn.trim();

		if(nome){
			var slt = this.followingUser;
			var resp = this.flp.transform(slt, nome);
			if(resp.length > 0){
				this.followingUser = resp;
			}else{
				event.target.value = event.target.value.slice(0,-1);
			}
		}else{
			this.ngOnInit();
		}
	}
}
