import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

	//public UserFind: string;
	public listUsers: Array<any> = [];
	public page = 1;
	public finished: boolean = true;
	user:any=JSON.parse(localStorage.getItem('user'));
	timeout:any=null;

	constructor(private userService: UserService){}

	ngOnInit(){
	    this.GetRecentUser();
	}

	UserFindByDisplayName(event: any){
    clearTimeout(this.timeout);
    var evn = event.target.value;
    var nome = evn.trim();
		this.page = 1;
		//this.listUsers = [];
		//this.GetUser(event.target.value);
		if(nome){
			let ts = this;
			this.timeout = setTimeout(function () {
				console.log(nome);
				ts.userService.GetUserByUsername(nome).subscribe(
					res => {
						if(res){
							ts.listUsers=[]
							ts.listUsers.push(res);
						}else{
							ts.listUsers=[]
						}
					},
					err => {
						console.log(err)
					})
			}, 500);
		}
	}

	onScroll(){
		this.page++;
		//this.GetUser();
	}

	addPage(){
		this.page++;
		//this.GetUser();
	}

	public GetUser(UserFind){
		let ts = this;
		
		this.userService.GetUser(this.page, UserFind).subscribe(
			res => {
				//res.users = res.users.filter((item) => item._id != ts.user._id);// remove o usuário logado
				if(res.users.length != 6){
					this.finished = false
				}
				if(this.listUsers.length == 0){
					this.listUsers = res.users;
				}else{
					res.users.map(item => {
						this.listUsers.push(item);
					});
				}
			},
			err => {
				console.log(err)
			})
		
	}

	public GetRecentUser(){
		let ts = this;
		this.userService.GetUser(1, "").subscribe(
		  res => {
		  	res.users = res.users.filter((item) => item._id != ts.user._id);// remove o usuário logado
		    if(res.users.length != 6){
		      this.finished = false
		    }
		    if(this.listUsers.length == 0){
		      this.listUsers = res.users;
		    }else{
		      res.users.map(item => {
		        this.listUsers.push(item);
		      });
		    }
		  },
		  err => {
		    console.log(err)
		  })
	}

}
