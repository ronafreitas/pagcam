import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../../services/group.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  user:any = JSON.parse(localStorage.getItem('user'));
  mygroups:any={}
  pagegp:number=0;
  id_group:number;
  showmodal:boolean=false;
  nomegrupo:string;
  constructor(private gs:GroupService){}

  ngOnInit(){
		this.gs.GetGroupByUserId(this.user._id,this.pagegp).subscribe(
		  res => {
        console.log(res);
        this.mygroups = res;
		  }, err => {
		    console.log(err)
      }
    );
  }

  GroupFindByName(event: any){
    console.log(event.target.value);
    
    /*let ts = this;
		setTimeout(function(){
			ts.GetUser(event.target.value);
    }, 1000);*/
    
  }

  modalDelete(idGroup:number,nameGroup:string){
    this.id_group = idGroup;
    this.nomegrupo = nameGroup;
    this.showmodal=true;
  }

  fechamodal(){
    this.showmodal=false;
  }

  confirmar(){
    let ts = this;
    ts.showmodal=false;
		ts.gs.DeleteGroupById(ts.id_group).subscribe(
		  res => { 
        console.log(res);
        ts.ngOnInit();
        //this.mygroups = res;
		  }, err => {
		    console.log(err)
      }
    );
  }

}
