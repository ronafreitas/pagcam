import { Component,OnInit,ViewChild } from '@angular/core';//ChangeDetectorRef private cd: ChangeDetectorRef
import { GroupService } from '../../../services/group.service';
import { FileService } from '../../../services/file.service';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { DomSanitizer } from '@angular/platform-browser';
import {Router,ActivatedRoute} from "@angular/router"
@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComponent implements OnInit {

	@ViewChild("fileInput") fileInput;
	@ViewChild('message') input:any; 
	public idgroup: any;
	public namegroup: any;
	imgupl:any;
	user:any=JSON.parse(localStorage.getItem('user'));
	filesToUpload: Array<any> = [];
	imguplupl:boolean=false;
	publicando:boolean=false;
	menspubl:string;
	uploadedImage: File;
	imagePreview: any;

	constructor(
		private gs:GroupService,
		private fileService: FileService,
		private ng2ImgMax: Ng2ImgMaxService,
		private router: Router,
		private route: ActivatedRoute,
		public sanitizer: DomSanitizer){}

	getImagePreview(file: File){
		const reader: FileReader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			this.imagePreview = reader.result;
		};
	}
	
	ngOnInit(){
		this.idgroup = this.route.snapshot.paramMap.get('id');
		this.namegroup = this.route.snapshot.paramMap.get('name');
	}

	onFileChange(event){
		let image = event.target.files[0];
		// tamanho da imagem 0.075 = 75Kb
		//this.ng2ImgMax.resizeImage(image, 50, 50).subscribe(
		this.ng2ImgMax.compressImage(image, 0.075).subscribe(
			result => {
				this.uploadedImage = new File([result], result.name);
				this.getImagePreview(this.uploadedImage);
			},
			error => {
				this.uploadedImage=null;
				alert('Não foi possível selecionar essa foto');
				console.log('Oh no!', error);
			}
		);
	}

	public AddPublication(e){
		e.target.style.display='none';
		this.publicando=true;
		this.menspubl = 'Aguarde...'
		let ts = this;
		let mensagem = this.input.nativeElement.value;
		var msg = mensagem.trim();
		let fi = this.fileInput.nativeElement;
		if(fi.files && fi.files[0]){
			let fileToUpload = fi.files[0];
			this.filesToUpload.push(fileToUpload);
		  
			this.fileService.AddFile(this.filesToUpload, this.user._id, 'group/publication').subscribe(
				res => {
					const newPubGroup={
						userId:this.user._id,
						groupId:this.idgroup,
						message:msg,
						like:[],
						filePublication:res,
						comment:[],
					}
					this.gs.AddPublication(newPubGroup).subscribe(
						ret => {
							ts.router.navigate(['/groups'])
						},
						err => {
							console.log(err)
						}
					)
				},
				err => {
					ts.menspubl = 'Erro ao tentar publicar, tente novamente'
				}
			)
		}else{
			if(msg != ""){
				let fileToUpload = [];
				const newPubGroup={
					userId:this.user._id,
					groupId:this.idgroup,
					message:msg,
					like:[],
					filePublication:fileToUpload,
					comment:[],
				}
				this.gs.AddPublication(newPubGroup).subscribe(
					ret => {
						ts.router.navigate(['/groups'])
					},
					err => {
						console.log(err)
					}
				)
			}else{
				ts.menspubl = 'Necessário selecionar uma foto ou escrever uma mensagem';
				e.target.style.display='block';
			}
		}
	}

}
