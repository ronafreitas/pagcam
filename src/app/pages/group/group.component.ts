import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../services/group.service';
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

  pagegp:number=0;
  groups:any=[]
  timeout:any=null;
  gpencont:boolean=false;

  constructor(private gs:GroupService){}

  ngOnInit(){
		this.gs.GetGroups(this.pagegp).subscribe(
		  res => {
        this.groups = res
		  }, err => {
		    console.log(err)
      }
    );
  }

  GroupFindByName(event: any){
    clearTimeout(this.timeout);
    var evn = event.target.value;
    var nome = evn.trim();
    if(nome){
      let ts = this;
      this.timeout = setTimeout(function () {
          ts.gs.GetGroupByName(event.target.value).subscribe(
          res => {
            console.log(res);
            if(res.length > 0){
              ts.groups = res;
              ts.gpencont=false;
            }else{
              ts.groups =[]
              ts.gpencont=true;
            }
            
          }, err => {
            console.log(err)
          }
        );
      }, 500);
    }else{
      this.ngOnInit();
      this.gpencont=false;
    }
  }

}
