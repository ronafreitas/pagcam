import { Component, OnInit,ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileService } from '../../../services/file.service';
import { GroupService } from '../../../services/group.service';
import {Router,ActivatedRoute} from "@angular/router"
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

	user:any = JSON.parse(localStorage.getItem('user'));
	registerForm: FormGroup;
	loading:boolean=false;
	imguplupl:boolean=false;
	submitted:boolean=false;
  imgupl:any;
  public id: any;
  @ViewChild("fileInput") fileInput;
  filesToUpload: Array<any> = [];

  constructor(
    private gs:GroupService,
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private fileService: FileService){}

  ngOnInit(){
    this.id = this.route.snapshot.paramMap.get('id');
    let ts = this;
    this.gs.GetGroupById(this.id).subscribe(
      ret => {
        ts.imgupl = ret[0].avatar;
        this.registerForm.get('name').setValue(ret[0].name)
      },
      err => {
        console.log(err)
      }
    )
    ts.registerForm = ts.formBuilder.group({
      name: ['', Validators.required]
    });
  }

	onFileChange(event){
		let reader = new FileReader();
		let ts = this;
		if(event.target.files && event.target.files.length){
			const [imagem] = event.target.files;
			reader.readAsDataURL(imagem);
			reader.onload = () => {
				ts.imguplupl=true;
				ts.imgupl = reader.result;
				//console.log(reader.result);
				// need to run CD since file load runs outside of zone
				this.cd.markForCheck();
			};
		}
  }

	get f() { return this.registerForm.controls; }

  onSubmit(){

    let userform = this.registerForm.value;
    let ts = this;    	
    this.submitted = true;

    if(this.registerForm.invalid){
        return;
    }

    ts.loading = true;
		let fi = this.fileInput.nativeElement;
		if(fi.files && fi.files[0]){
			let fileToUpload = fi.files[0];
			this.filesToUpload.push(fileToUpload);
			this.fileService.AddFile(this.filesToUpload, this.user._id, 'group/group').subscribe(
				res => {
          var avt = res[0].url;
          const upGroup={
            _id:ts.id,
            id_user_create:this.user._id,
            name:userform.name,
            avatar:avt,
            description:'-',
            users:[],
            admin:[ts.user._id]
          }
          var avt = res[0].url;
					this.gs.EditGroupById(upGroup).subscribe(
					  ret => {
              ts.loading=false;
              ts.router.navigate(['/list-groups'])
					  },
					  err => {
              ts.loading=false;
					    console.log(err)
					  }
          )
				},
				err => {
          ts.loading=false;
					console.log(err);
					//e.target.nextElementSibling.innerHTML='Erro ao tentar publicar, tente novamente';
				}
			)
		}else{
      const upGroup={
        _id:ts.id,
        id_user_create:this.user._id,
        name:userform.name,
        avatar:ts.imgupl,
        description:'-',
        users:[],
        admin:[ts.user._id]
      }
      this.gs.EditGroupById(upGroup).subscribe(
        ret => {
          ts.loading=false;
          ts.router.navigate(['/list-groups'])
        },
        err => {
          ts.loading=false;
          console.log(err)
        }
      )
		}
  }
}
