import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupService } from '../../../services/group.service';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  tipo:number=2;
  listPublications: Array<any> = [];

  public id: string;
  public imgupl:any;
  public namegroup:any;
  constructor(private gs:GroupService,private route: ActivatedRoute){}

  ngOnInit(){
    let ts = this;
    this.listPublications=[]
    this.id = this.route.snapshot.paramMap.get('id');
    this.gs.GetGroupById(this.id).subscribe(
      ret => {
        this.imgupl = ret[0].avatar;
        this.namegroup = ret[0].name;

        ts.gs.GetPublicationsByGroupId(ret[0]._id).subscribe(
          ret => {
            if(this.listPublications.length == 0){
              this.listPublications = ret.publications;
            }
          },
          err => {
            console.log(err)
          }
        )
      },
      err => {
        console.log(err)
      }
    )
  }

}
