import { Component, OnInit,ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileService } from '../../../services/file.service';
import { GroupService } from '../../../services/group.service';
import {Router} from "@angular/router"
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

	user:any = JSON.parse(localStorage.getItem('user'));
	registerForm: FormGroup;
	loading:boolean=false;
	imguplupl:boolean=false;
	submitted:boolean=false;
	savegroup:boolean=false;
	imgupl:any;
  @ViewChild("fileInput") fileInput;
  filesToUpload: Array<any> = [];

  constructor(
    private gs:GroupService,
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private router: Router,
    private fileService: FileService){}

  ngOnInit(){
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

	onFileChange(event){
		let reader = new FileReader();
		let ts = this;
		if(event.target.files && event.target.files.length){
			const [imagem] = event.target.files;
			reader.readAsDataURL(imagem);
			reader.onload = () => {
				ts.imguplupl=true;
				ts.imgupl = reader.result;
				//console.log(reader.result);
				// need to run CD since file load runs outside of zone
				this.cd.markForCheck();
			};
		}
  }

	get f() { return this.registerForm.controls; }

  onSubmit(){

    let userform = this.registerForm.value;
    let ts = this;    	
    ts.submitted = true;
    ts.savegroup = false;
    if(this.registerForm.invalid){
        return;
    }

    ts.loading = true;
		let fi = this.fileInput.nativeElement;
		if(fi.files && fi.files[0]){
			let fileToUpload = fi.files[0];
			this.filesToUpload.push(fileToUpload);
			this.fileService.AddFile(this.filesToUpload, this.user._id, 'group/group').subscribe(
				res => {
          var avt = res[0].url;
          const newGroup={
            id_user_create:this.user._id,
            name:userform.name,
            avatar:avt,
            description:'',
            users:[],
            admin:[ts.user._id]
          }
          var avt = res[0].url;
					this.gs.AddGroup(newGroup).subscribe(
					  ret => {
              ts.loading=false;
              ts.router.navigate(['/groups'])
              console.log(ret)
					  },
					  err => {
              ts.loading=false;
					    console.log(err)
					  }
          )
				},
				err => {
          ts.loading=false;
					console.log(err);
					//e.target.nextElementSibling.innerHTML='Erro ao tentar publicar, tente novamente';
				}
			)
		}else{
      ts.loading = false;
      ts.savegroup = true;
		}
  }
}
