import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

//import { AlertService, UserService, AuthenticationService } from '@app/_services';
//import { UserService } from '@app/services';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        //private router: Router,
       // private authenticationService: AuthenticationService,
        private loginS: LoginService,
       // private alertService: AlertService
    ) { 
        
        // redirect to home if already logged in
        
        /*if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/']);
        }*/

        localStorage.clear();

        /*let getuser = JSON.parse(localStorage.getItem('user'));
        if(getuser != undefined || getuser != null){
            console.log('YUYUYUY');
            window.location.href = '/home';
        }else{
            console.log('VUKCAS',getuser);
        }*/
    }

    ngOnInit(){
        //localStorage.clear();
        this.registerForm = this.formBuilder.group({
            displayName: ['', Validators.required],
            username: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {

    	let user = this.registerForm.value;      	
      	user.verificationPassword = user.password;
      	user.providerId = 'email.com';
        /*var dt = new Date();
        var h = dt.getHours();
        var m = dt.getMinutes();
        var s = dt.getUTCMilliseconds();
        var bsd = h+''+m+''+s;
        user.avatar = `https://api.adorable.io/avatars/68/${bsd}.png`;*/
      	user.avatar = './assets/imgs/catavatar.jpg';

        this.submitted = true;

        // stop here if form is invalid
        if(this.registerForm.invalid){
            return;
        }

        this.loading = true;
        
        this.loginS
        .register(user)
        .pipe(first())
        .subscribe(
            data => {
            	//alert(data.message);
                //this.alertService.success('Registration successful', true);
                //this.router.navigate(['/home']);
                window.location.href = '/login';
            },
            error => {
            	alert(error.error.message);
                //this.alertService.error(error);
                this.loading = false;
            });
    }
}
