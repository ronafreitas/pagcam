import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
//import { NgbdTabsetBasic } from '../../tabset-basic';

//import { PublicationService } from '../../services/publication.service';
import { UserService } from '../../services/user.service';
import { FollowerService } from '../../services/follower.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

	public user: any = {};
	public userinsession: any = JSON.parse(localStorage.getItem('user'));
	value: string;
	public followerList: Array<any> = []

	constructor(
		private acroute: ActivatedRoute,
		private router: Router,
		private userService: UserService,
		private followerService: FollowerService,
	){}

	ngOnInit(){
		//let param1 = this.route.snapshot.queryParams["id"];
		let thisuser = this.acroute.snapshot.params;
		//console.log('parametro',thisuser.id);

		if(thisuser.id == this.userinsession._id){
			this.router.navigate(['/profile']);
		}else{
			this.GetUserProfile(thisuser.id);
			this.GetFollowerByUserId(this.userinsession._id);
		}

	}

	public async Follow(userId, followerId) {
		const data = { userId, followerId }
		this.followerService.AddFollower(data).subscribe(
		  res => {
		  	//console.log(res);
		    //this.socketservice.AddFollower(res, this.user)
		    //this.AddNotification(followerId,`@${this.user.username} ha comenzado a seguirte`)
		  },
		  err => {
		    console.log(err)
		  })
	}

	public UnFollower(userId, followerId) {
		const data = { userId, followerId }
		this.followerService.RemoveFollower(data).subscribe(
		  res => {
		  	//console.log(res);
		  },
		  err => {
		    console.log(err)
		  })
	}

	public Follow_And_UnFollow(e,followerId){
		if(!e.target.classList.contains('btn_user_unflw')){
			e.target.classList.remove('btn_user_flw');
			e.target.classList.add('btn_user_unflw');
			e.target.innerHTML = '<span class="ion ion-md-person"></span>  Seguindo';
			this.Follow(this.userinsession._id, followerId);
		}else{
			e.target.classList.remove('btn_user_unflw');
			e.target.classList.add('btn_user_flw');
			e.target.innerHTML = '<span class="ion ion-md-person"></span>  Seguir';
			this.UnFollower(this.userinsession._id, followerId);
		}
	}

	checkflw(userId){
		const x = this.followerList.filter(item => item.followerId._id == userId);
		return x.length > 0 ? true : false;
	}

	//onSelect(data: TabDirective): void {
	onSelect(data, num:number){
		console.log(data);
		console.log(num);
		//this.value = data.heading;
	}

	public GetFollowerByUserId(userid) {
		this.followerService.GetFollowerByUserId(userid).subscribe(res => {
		  this.followerList = res.response;
		  //console.log(res);
		}, err => {

		})
	}

	GetUserProfile(userid){
		/*this.acroute.params.subscribe(params => {
		  //this.userService.GetUserByUsername(params.username).subscribe(
		  this.userService.GetUserById(params.username).subscribe(
		    res => {
		      console.log(res);
		      this.user = res;
		      //this.GetFollowerByUserId();
		      //this.GetFollowingByUserId();
		      //this.GetPublicationByUserId();
		      //this.GetPublicationByFollowerUserId();
		    },
		    err => {
		      console.log(err)
		    })
		})*/

		this.userService.GetUserById(userid).subscribe(
			res => {
			  //console.log(res);
			  this.user = res;
			  //this.GetFollowerByUserId();
			  //this.GetFollowingByUserId();
			  //this.GetPublicationByUserId();
			  //this.GetPublicationByFollowerUserId();
			},
			err => {
			  console.log(err)
			}
		)
	}

	rediMess(objUser){
		localStorage.removeItem('this_user_chat');
		localStorage.setItem('this_user_chat',JSON.stringify(objUser));
		this.router.navigate(['/messages']);
	}
}
