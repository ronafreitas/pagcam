import { Component, OnInit } from '@angular/core';
import { FollowerService } from '../../services/follower.service';
import { PhotoService } from '../../services/photo.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

	user:any=JSON.parse(localStorage.getItem('user'));
	followinsgcount:any=0;
	followerscount:any=0;
	photocount:any=0;
	photopublications: [];

	constructor(private followerService: FollowerService,private photoService: PhotoService){}

	ngOnInit(){
		this.followerService.GetCountFollowingsByUserId(this.user._id).subscribe(
		  res => {
		    this.followinsgcount = res.total;
		  }, err => {
		    console.log(err)
		  })

		this.followerService.GetCountFollowersByUserId(this.user._id).subscribe(
		  res => {
		    this.followerscount = res.total;
		  }, err => {
		    console.log(err)
		  })

		this.photoService.GetPhotosByUserId(this.user._id).subscribe(
		  res => {
		    this.photocount = res.total;
		    this.photopublications = res.publications;
		  }, err => {
		    console.log(err)
		  })
	}
}
