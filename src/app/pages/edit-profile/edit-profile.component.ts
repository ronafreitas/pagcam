import { Component, OnInit,ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileService } from '../../services/file.service';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

	user:any = JSON.parse(localStorage.getItem('user'));
	registerForm: FormGroup;
	loading:boolean=false;
	imguplupl:boolean=false;
	submitted:boolean=false;
	saveprof:boolean=false;
	imgupl:any;
	@ViewChild("fileInput") fileInput;
	filesToUpload: Array<any> = [];

	constructor(
		private formBuilder: FormBuilder,
		private fileService: FileService,
		private cd: ChangeDetectorRef,
		private userService: UserService
		){}

	ngOnInit(){
        this.registerForm = this.formBuilder.group({
            displayName: [this.user.displayName, Validators.required],
            presentation: [this.user.presentation, [Validators.required]]
        });
        this.imgupl = this.user.avatar;
	}

	onFileChange(event) {
		let reader = new FileReader();
		let ts = this;
		if(event.target.files && event.target.files.length){
			const [imagem] = event.target.files;
			reader.readAsDataURL(imagem);
			reader.onload = () => {
				ts.imguplupl=true;
				ts.imgupl = reader.result;
				//console.log(reader.result);
				// need to run CD since file load runs outside of zone
				this.cd.markForCheck();
			};
		}
	}

	get f() { return this.registerForm.controls; }

    onSubmit(){

    	let userform = this.registerForm.value;
    	let ts = this;    	
        this.submitted = true;

        if(this.registerForm.invalid){
            return;
        }

        ts.loading = true;
		let fi = this.fileInput.nativeElement;
		if(fi.files && fi.files[0]){
			let fileToUpload = fi.files[0];
			this.filesToUpload.push(fileToUpload);
			this.fileService.AddFile(this.filesToUpload, this.user._id, 'profile').subscribe(
				res => {
					var avt = res[0].url;
					const UserUp = {
						avatar:avt,
						displayName:userform.displayName,
						presentation:userform.presentation
					}
					this.userService.UpdateProfile(this.user._id,UserUp).subscribe(
					  ret => {
							ts.user.avatar = UserUp.avatar;
							ts.user.displayName = userform.displayName;
							ts.user.presentation = userform.presentation;
							localStorage.setItem("user", JSON.stringify(ts.user));
							ts.loading = false;
							ts.saveprof = true;
					  },
					  err => {
					    console.log(err)
					  }
					)
				},
				err => {
					console.log(err);
					//e.target.nextElementSibling.innerHTML='Erro ao tentar publicar, tente novamente';
				}
			)
		}else{
			const UserUp = {
				displayName:userform.displayName,
				presentation:userform.presentation
			}
			this.userService.UpdateProfile(this.user._id,UserUp).subscribe(
			  ret => {
					ts.user.displayName = userform.displayName;
					ts.user.presentation = userform.presentation;
					localStorage.setItem("user", JSON.stringify(ts.user));
					ts.loading = false;
					ts.saveprof = true;
			  },
			  err => {
			    console.log(err)
			  }
			)
		}
    }
}
