import { Component, OnInit } from '@angular/core';
import { PublicationService } from '../../services/publication.service';

import { UserService } from '../../services/user.service';
import { Publication } from '../../models/publication.model';
//import { User } from '../../models/user.model';
@Component({
	selector: 'app-home',
	template:`
		<div *ngIf="loading" class="homeload"></div>
		<feed [tipo]=tipo [(objectLIst)]="listPublications"></feed>
	`
	//<feed [views]="valor" (objectLIstChange)="valor.views = $event"></feed>
})
export class HomeComponent implements OnInit {

	//public valor:any = []
	tipo:number=1;
	user:any = JSON.parse(localStorage.getItem('user'));
	listPublications: Array<Publication> = [];
	page: number = 1;
	page2: number = 1;
	//publicationTotal: number;
	finished: boolean = true;
	loading: boolean = true;
	
	stylike_def: string = '#aba9a9';
	constructor(
		private publicationService: PublicationService,
		private userService: UserService
	){}

	ngOnInit(){
		//this.valor.push({teste:'s7s7s'});
		this.GetPublicationByUserId();
		//this.GetPublicationByFollowerUserId();
	}

	onScroll(){
		this.page++;
		this.GetPublicationByUserId();
		//this.GetPublicationByFollowerUserId();
	}

	public GetPublicationByUserId(){
		this.publicationService.GetPublicationByUserId(this.user._id, this.page).subscribe(
		  res => {
				console.log(res);
		    //this.publicationTotal = res.total
		    if(res.publications.length != 6) {
		      this.finished = false
		    }
				if(this.listPublications.length == 0){
					this.listPublications = res.publications;
				}


				//GetPublicationByFollowerUserId

		    this.loading=false;
		    if(!localStorage.getItem('o')){
					this.userService.UpdateProfile(this.user._id,{online:true}).subscribe(ret => {
						localStorage.setItem('o','1');
					},err => {});
		    }
		  },
		  err => {
		    console.log(err)
			}
		)
	}

	public GetPublicationByFollowerUserId(){
		this.publicationService.GetPublicationFollowersByUserId(this.user._id, this.page).subscribe(
		  res => {
		    res.publications.forEach(publication => {
		      this.listPublications.push(publication)
		    })
		  },
		  err => {
		    console.log('GetPublicationByFollowerUserId');
		    console.log(err);
			}
		)
	}

}
