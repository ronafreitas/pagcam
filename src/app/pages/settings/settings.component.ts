import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  registerForm: FormGroup;
  submitted:boolean=false;
  passchang:boolean=false;
  user:any=JSON.parse(localStorage.getItem('user'));
  textosenha:any;
  constructor(private formBuilder: FormBuilder,private userService: UserService){}

  ngOnInit(){
    this.registerForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

	get f() { return this.registerForm.controls; }

  onSubmit(){
    this.submitted = true;
    let userform = this.registerForm.value;
    let ts = this;    	
    
    if(this.registerForm.invalid){
      return false;
    }

    this.userService.UpdateUsersOnlyPassword(this.user._id,{password:userform.password}).subscribe(
      ret => {
        ts.passchang = true;
        ts.textosenha = 'Senha alterara com sucesso';
      },
      err => {
        ts.passchang = true;
        ts.textosenha = 'NÃO foi possível alterar sua SENHA';
      }
    )
  }

  desativar(){
    var con = confirm("Confirma exclusão da conta?");
    if(con == true){
      this.userService.DeactivateAccount(this.user._id).subscribe(
        ret => {
          localStorage.clear();
          //console.log(ret);
          window.location.href = '/login';
        },
        err => {
          localStorage.clear();
          //console.log(err);
          window.location.href = '/login';
        }
      )
    }
  }
}
