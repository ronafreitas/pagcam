import { Component,ViewChild } from '@angular/core';//ChangeDetectorRef private cd: ChangeDetectorRef
import { PublicationService } from '../../services/publication.service';
import { FileService } from '../../services/file.service';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.css']
})
export class PublishComponent {
	@ViewChild("fileInput") fileInput;
	@ViewChild('message') input:any; 
	tipo:any=1;
	tipopub:any=1;

	imgupl:any;
	user:any=JSON.parse(localStorage.getItem('user'));
	filesToUpload: Array<any> = [];
	imguplupl:boolean=false;
	publicando:boolean=false;
	menspubl:string;
	uploadedImage: File;
	imagePreview: any;
	imageThumb:any;

	//registerForm: FormGroup;
	constructor(
		private publicationService: PublicationService,
		private fileService: FileService,
		private ng2ImgMax: Ng2ImgMaxService,
		public sanitizer: DomSanitizer){}

	getImagePreview(file: File){
		const reader: FileReader = new FileReader();
		let ts = this;
		reader.readAsDataURL(file);
		reader.onload = () => {
			this.imagePreview = reader.result;
		};
	}

	/*
	thumbnailify(base64Image, targetSize, callback) {
		var img = new Image();
	  
		img.onload = function() {
		  var width = img.width,
			  height = img.height,
			  canvas = document.createElement('canvas'),
			  ctx = canvas.getContext("2d");
	  
		  //canvas.width = canvas.height = targetSize;
	  
		  ctx.drawImage(
			img,
			width > height ? (width - height) / 2 : 0,
			height > width ? (height - width) / 2 : 0,
			width > height ? height : width,
			width > height ? height : width,
			0, 0,
			targetSize, targetSize
		  );
	  
		  callback(canvas.toDataURL());
		};
	  
		img.src = base64Image;
	};
	ts.thumbnailify(reader.result, 100, function(base64Thumbnail) {
		//console.log(base64Thumbnail);
		ts.imagePreview2 = base64Thumbnail
	});
	*/
	
	onFileChange(event){
		let image = event.target.files[0];

		// tamanho da imagem 0.075 = 75Kb
		//this.ng2ImgMax.resizeImage(image, 50, 50).subscribe(
		this.ng2ImgMax.compressImage(image, 0.075).subscribe(
			result => {
				this.uploadedImage = new File([result], result.name);
				this.getImagePreview(this.uploadedImage);
			},
			error => {
				console.log('Oh no!', error);
			}
		);
	}

	setTipo(vl){
		this.tipo = vl;
	}

	setTipoPub(tp){
		this.tipopub = tp;
	}
	public AddPublication(e){
		e.target.style.display='none';
		this.publicando=true;
		this.menspubl = 'Aguarde...'
		let ts = this;
		let legenda = this.input.nativeElement.value;
		//let legenda = document.getElementById('message').value;

		let user = {
			avatar: this.user.avatar,
			_id: this.user._id,
			displayName: this.user.displayName
		}
		
		let fi = this.fileInput.nativeElement;
		if(fi.files && fi.files[0]){
			let fileToUpload = fi.files[0];
			this.filesToUpload.push(fileToUpload);
			this.fileService.AddFile(this.filesToUpload, this.user._id, 'publ/feed').subscribe(
				res => {
					var lgd = legenda.trim();
					let publication = {message:lgd,userId:ts.user._id,filePublication:res, type:ts.tipo, local:ts.tipopub};
					ts.publicationService.AddPublication(publication).subscribe(
					res => {
						//e.target.nextElementSibling.innerHTML='Publicado com sucesso';
						ts.menspubl = 'Publicado com sucesso'
						ts.filesToUpload = [];
						//this.progress.complete()
					},
					err => {
						console.log(err)
						// this.progress.complete()
					})
				},
				err => {
					console.log(err);
					//e.target.nextElementSibling.innerHTML='Erro ao tentar publicar, tente novamente';
					ts.menspubl = 'Erro ao tentar publicar, tente novamente'
				}
			)
		}else{
			var lgd = legenda.trim();
			if(lgd != ""){
				let fileToUpload = [];
				let publication = {message:lgd,userId:this.user._id,filePublication:fileToUpload, type:this.tipo, local:this.tipopub };
	
				this.publicationService.AddPublication(publication).subscribe(
				res => {
					ts.menspubl = 'Publicado com sucesso'
				},
				err => {
					console.log(err.error);
					ts.menspubl = 'Erro ao tentar publicar, tente novamente'
				})
			}else{
				ts.menspubl = 'Necessário selecionar uma foto ou preencher a legenda';
				e.target.style.display='block';
			}
		}
	}

}
