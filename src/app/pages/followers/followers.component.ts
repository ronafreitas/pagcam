import { Component, OnInit } from '@angular/core';
import { FollowerService } from '../../services/follower.service';
import { FilterUserPipeModule } from '../../pipes/filteruser.pipe';
@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
	styleUrls: ['./followers.component.css'],
	providers: [ FilterUserPipeModule ]
})
export class FollowersComponent implements OnInit {

	public userinsession: any = {};
	public followerUser: Array<any> = [];
	public followerTotal: number=0;

	constructor(
		private followerService: FollowerService,
		private flp:FilterUserPipeModule
	){
		this.userinsession = JSON.parse(localStorage.getItem('user'));
	}

	ngOnInit(){
		this.GetFollowingByUserId();
	}

	// estou sendo seguido por 'USER'
	// mudar nome do método, ta invertido 'followers' <=> 'following'
	GetFollowingByUserId() {
		this.followerService.GetFollowingByUserId(this.userinsession._id).subscribe(
		  res => {
		    this.followerTotal = res.total
		    this.followerUser = res.response.map(item => {
		      return item.userId
		    })
		  }, err => {
		    console.log(err)
		  })
	}

	filterUser(event:any){
    var evn = event.target.value;
    var nome = evn.trim();

		if(nome){
			var slt = this.followerUser;
			var resp = this.flp.transform(slt, nome);
			if(resp.length > 0){
				this.followerUser = resp;
			}else{
				event.target.value = event.target.value.slice(0,-1);
			}
		}else{
			this.ngOnInit();
		}
	}
	
}
