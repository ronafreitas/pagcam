import { Component,ElementRef,ViewChild } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { ChatlistService } from '../../services/chatlist.service';
import { SocketService } from '../../services/socket.service';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent {

	public userinsession: any = JSON.parse(localStorage.getItem('user')) || null;
	public user_chat: any = JSON.parse(localStorage.getItem('this_user_chat')) || {n:1};
	public usersChats: any = [];
	//public usersChats: any = JSON.parse(localStorage.getItem('all_chat')) || [];
	conversa:any = [];
	conversa_count:any = [];
	keysconversa:any;
	initichatload:boolean=false;
	initichatlink:boolean=false;
	primeiro_contato:boolean=true;
	disabled:boolean=true;
	setgoup:boolean=false; 
	setgodown:boolean=false;
	iduserChat:any;
	pagchatlist:number=0;
	pagchat:number=0;
	@ViewChild('scrollMe') private myScrollContainer: ElementRef;
	
	constructor(
		public chatService:ChatService,
		public chatlistService:ChatlistService,
		public socketservice: SocketService){

		let ts = this;
		if(this.user_chat.n){
			ts.chatlistService.GetChatlistByUserId(this.pagchatlist,ts.userinsession._id).subscribe(
			res => {
				res.forEach(item => {
					ts.usersChats = item.chats;
				});
			},
			err => {
				console.log(err);
			});
		}else{
		    localStorage.removeItem('this_user_chat');
			const body = {
				_id:this.userinsession._id,
				chats:{
					creationDate: new Date(),
					user_id: this.user_chat._id,
					user_avatar: this.user_chat.avatar,
					user_displayName: this.user_chat.displayName
				}
			}
			this.chatlistService.AddChatlist(body).subscribe(
			res => {
				ts.chatlistService.GetChatlistByUserId(0,ts.userinsession._id).subscribe(
				res => {
					res.forEach(item => {
						ts.usersChats = item.chats;
					});
				},
				err => {
					console.log(err);
				});
			},
			err => {
				console.log(err);
			});
		}

		// atualiza o chat em tempo real
		this.socketservice.onSocket().subscribe(res => {
			if(ts.conversa_count[res.idUserFrom]){
				ts.conversa_count[res.idUserFrom] = 1;
				console.log(ts.conversa_count[res.idUserFrom]);
			}else{
				var vlrcont = parseInt(ts.conversa_count[res.idUserFrom]);
				console.log(vlrcont);
				ts.conversa_count[res.idUserFrom] = (vlrcont+1);
				console.log(ts.conversa_count[res.idUserFrom]);
			}
			document.getElementById('cont_user_'+res.idUserFrom).innerHTML = ts.conversa_count[res.idUserFrom]
			ts.conversa.push({text:res.textMes,userFromChat:res.idUserFrom});
			ts.setgodown=true;
		});
	} 

	initChat(idUser){
		var elemPai = (<HTMLInputElement>document.getElementById('chlist_pai'));
		elemPai.classList.remove('active_chat');
		var elem = (<HTMLInputElement>document.getElementById('usrch_'+idUser));
		
		//@ts-ignore TS2339
		elem.parentNode.classList.remove('active_chat');

		/*if(elem.parentNode){
			if(!elem.parentNode.classList.contains('active_chat')){
				elem.parentNode.classList.add('active_chat');
			}else{

			}
		}*/

		let ts = this;
		ts.conversa = [];
		this.initichatload=true;
		this.initichatlink=false;

      	this.chatService.GetMessageChat(this.pagchat, this.userinsession._id+'-'+idUser).subscribe(
        res => {
        	
        	if(res){
				if(res.Messages.length > 0){
					let cts = res.Messages;
					this.keysconversa = Object.keys(cts);
					this.keysconversa.sort( function ( a, b ) { return b - a; } )
					for(var i = 0; i < this.keysconversa.length; i++ ){
						ts.conversa.push(cts[ this.keysconversa[i] ]);
					}
					if(res.Messages.length > this.pagchat){
						ts.setgoup=true;
					}else{
						ts.setgoup=false;
					}
					setTimeout(function(){ts.scrollToBottom();}, 300);
					//this.pagchat = 5;
				}
        	}
			ts.initichatload=false;
			ts.disabled=false;
			ts.initichatlink=true;
			ts.iduserChat=idUser;
        },
        err => {
          console.log(err);
        })
	}
	
	goUp(idUser){ 
		//this.setgoup
		let ts = this;
		
		console.log(this.pagchat);
		this.pagchat = (this.pagchat+5);
		console.log(this.pagchat);
      	this.chatService.GetMessageChat(this.pagchat, this.userinsession._id+'-'+idUser).subscribe(
        res => {
        	console.log(res.Messages);

        	if(res.Messages.length > 0){
				let cts = res.Messages;
				cts.forEach(item => {
					ts.conversa.unshift(item);
				});
				if(res.Messages.length > this.pagchat){
					ts.setgoup=true;
				}else{
					ts.setgoup=false;
				}
				
				console.log(res.Messages.length , this.pagchat);

				/*var keys = Object.keys(cts);
				keys.sort( function ( a, b ) { return b - a; } )
				for(var i = 0; i < keys.length; i++ ){
				    ts.conversa.unshift(cts[ keys[i] ]);
				}*/

				setTimeout(function(){ts.scrollToTop();}, 300);
        	}
			ts.initichatload=false;
			ts.disabled=false;
			ts.initichatlink=true;
			ts.iduserChat=idUser;
        },
        err => {
          console.log(err);
        })
	}
	goDwon(){
		this.setgodown=false;
		this.scrollToBottom();
	}
	enviarmsg(e,idUser){
		var msg = e.target.value;
		if(msg){

		    let ts = this;
		    ts.conversa.push({text:msg,userFromChat:this.userinsession._id});

		    if(this.primeiro_contato){
		    	// adiciona no OUTRO usuário um chatlist
				const createToChLt = {
					_id:idUser,
					chats:{
						creationDate: new Date(),
						user_id: this.userinsession._id,
						user_avatar: this.userinsession.avatar,
						user_displayName: this.userinsession.displayName
					}
				}
				this.chatlistService.AddChatlist(createToChLt).subscribe(
				res => {},
				err => {
					console.log(err);
				});
		    }
		    this.primeiro_contato=false;

		    // add nova mensagem MINHA na colletcion 'chat'
		    var addMeChat = {
		    	_id:this.userinsession._id+'-'+idUser,
		    	Messages:{
					text: msg,
					userFromChat: this.userinsession._id
		    	}
		    }
	      	this.chatService.AddMessage(addMeChat).subscribe(
	        res => { 
				ts.socketservice.SendMessage(ts.userinsession._id,idUser,msg);
				ts.scrollToBottom();
	        },
	        err => {
	          console.log(err);
	        });
	        
	        // add nova mensagem DO OUTRO na colletcion 'chat'
		    var addMeChat = {
		    	_id:idUser+'-'+this.userinsession._id,
		    	Messages:{
					text: e.target.value,
					userFromChat: this.userinsession._id
		    	}
		    }
	      	this.chatService.AddMessage(addMeChat).subscribe(res => {},err => {console.log(err);})

		}
		e.target.value = '';
	}

    scrollToBottom(): void {
        try{
        	this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }catch(err){
        	console.log(err);
        }
    }
	
    scrollToTop(): void {
        try{
        	this.myScrollContainer.nativeElement.scrollTop = 0;
        }catch(err){
        	console.log(err);
        }
    }
}
