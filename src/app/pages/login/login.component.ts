import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { LoginService } from '../../services/login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    registerForm: FormGroup;
    loading:boolean = false;
    submitted:boolean = false;
    shcom:boolean;

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		private loginS: LoginService
	) {
        localStorage.clear();
        
        /*console.log(localStorage.getItem('user'));

        if( localStorage.getItem('user') == 'undefined' || localStorage.getItem('user') == 'null' ){
            console.log('aaaaaaaaa');
            //window.location.href = '/home';
            
            this.shcom=true;
            this.router.navigate(['/home']);
        }*/
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            account: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    get f() { return this.registerForm.controls; }

    onSubmit() {

    	let user = this.registerForm.value;
        let ts = this;    	
        this.submitted = true;

        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        
        this.loginS
        .login(user)
        .pipe(first())
        .subscribe(
            data => {
				if(data.token != undefined || data.token != null){
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('user', JSON.stringify(data.user));
                    localStorage.setItem('l', '1');
                    window.location.href = '/home';
				}else{
					alert('Erro ao tentar acessar, tente novamente.');
                    this.loading = false;
				}
            },
            error => {
                alert('Não foi possível acessar, tente novamente');
            	console.log(error);
                //this.alertService.error(error);
                this.loading = false;
            });
    }

	/*Login(dataForm) {

		this.InitialValidationForm();

		if (this.InvalidValidation(dataForm.value)) {

		  const account_and_password = dataForm.value

		  this.loginService.Login(account_and_password).subscribe(res => {

		    if (res.token != undefined || res.token != null) {
		      localStorage.setItem('token', res.token)
		      localStorage.setItem('user', JSON.stringify(res.user))
		      window.location.href = '/home'
		    }

		    this.statusLogin = {
		      status: res.status, code: res.code, message: 'Verifica tu cuenta mediante el link que te envíamos al correo electrónico.'
		    }

		  }, err => {
		    this.statusLogin = {
		      status: err.error.status, code: err.error.code, message: 'Usuario no encontrado'
		    }
		  })
		}
	}*/
}
