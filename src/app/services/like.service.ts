import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';

@Injectable()
export class LikeService {

  private urlApi = urlApi;
  private headers: HttpHeaders;
  private token: string;

  constructor(private http: HttpClient){
    this.token = localStorage.getItem('token')
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.token);
  }

  //feed
  public AddLikeFeed(data:any): Observable<any> {
    return this.http.post(`${this.urlApi}/like`, data, { headers: this.headers });
  }
  public RemoveLikeFeed(data:any): Observable<any> {
    return this.http.post(`${this.urlApi}/unlike`, data, { headers: this.headers });
  }
  public GetLikeByPublicationId(publicationId:any): Observable<any> {
    return this.http.get(`${this.urlApi}/like/${publicationId}`, { headers: this.headers });
  }

  //groups
  public RemoveLikeGroup(data:any): Observable<any> {
    return this.http.post(`${this.urlApi}/group/unlike`, data, { headers: this.headers });
  }
  public AddLikeGroup(data:any): Observable<any> {
    return this.http.post(`${this.urlApi}/group/like`, data, { headers: this.headers });
  }
  public GetLikeByGroupPublicationId(publicationId:any): Observable<any> {
    return this.http.get(`${this.urlApi}/group/like/${publicationId}`, { headers: this.headers });
  }
}