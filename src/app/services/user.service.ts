import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';
@Injectable()
export class UserService {

  public urlApi: string = urlApi;
  public headers: HttpHeaders;

  constructor(
    private http: HttpClient
  ) {

    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', localStorage.getItem('token'));
  }

  public GetToken(username): Observable<any> {
    return this.http.get(`${this.urlApi}/token?username=${username}`, { headers: this.headers });
  }

  public GetUser(page = 0, displayname = ""): Observable<any> {
    return this.http.get(`${this.urlApi}/user?page=${page}&displayname=${displayname}`, { headers: this.headers });
  }

  public GetUserById(userId): Observable<any> {
    return this.http.get(`${this.urlApi}/user/id/${userId}`, { headers: this.headers });
  }

  public GetUserByUsername(username): Observable<any> {
    return this.http.get(`${this.urlApi}/user/username/${username}`, { headers: this.headers });
  }

  public UpdateProfile(id, profile): Observable<any> {
    return this.http.put(`${this.urlApi}/user/${id}`, profile, { headers: this.headers });
  }

  public UpdateUsersOnlyPassword(id, password): Observable<any> {
    return this.http.put(`${this.urlApi}/user/password/${id}`, password, { headers: this.headers });
  }

  public DeactivateAccount(iduser:string): Observable<any> {
    return this.http.delete(`${this.urlApi}/user/${iduser}`, { headers: this.headers });
  }
}