import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { urlApi } from '../config/index';
const SERVER_URL = urlApi.replace('/api', '');
import * as io from 'socket.io-client';
@Injectable()
export class SocketService {

  private url = SERVER_URL;  
  public usersct: any = JSON.parse(localStorage.getItem('user')) || {};
  private socket;
  constructor(){}

  emitSocket(data){
    this.socket = io(this.url);
    this.socket.emit('tpa_socket', data);    
  }

  onSocket(): Observable<any> {
    return new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('emit_me_'+this.usersct._id, (data) => {
        observer.next(data);    
      });
      return () => {
        this.socket.disconnect();
      };  
    })     
  }

  public SendMessage(idUserFrom,idUserTo, textMes): void {
    this.socket.emit('message_to', {idUserFrom,idUserTo,textMes});
  }

}
