import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';

@Injectable()
export class PhotoService {

  private urlApi: string = urlApi;
  private headers: HttpHeaders;
  private token: string;

  constructor(private http: HttpClient){

    this.token = localStorage.getItem('token')
    
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.token);

  }

  public GetPhotosByUserId(userId): Observable<any> {
    return this.http.get(`${this.urlApi}/publication/photos/${userId}`, { headers: this.headers });
  }
  
}