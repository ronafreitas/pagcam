import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';

@Injectable()
export class ChatService {

  private urlApi = urlApi;
  private headers: HttpHeaders;
  private token: string;

  constructor(private http: HttpClient) {

    this.token = localStorage.getItem('token')

    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.token);

  }

  public GetMessageChat(page=0, myId_UserId): Observable<any> {
    return this.http.get(`${this.urlApi}/chat/${myId_UserId}?page=${page}`, { headers: this.headers });
    //return this.http.get(`${this.urlApi}/comment/${publicationId}?page=${page}`, { headers: this.headers });
  }

  public AddMessage(message): Observable<any> {
    return this.http.post(`${this.urlApi}/chat`, message, { headers: this.headers });
  }

}