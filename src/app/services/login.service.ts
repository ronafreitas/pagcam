import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';

@Injectable()
//@Injectable({ providedIn: 'root' })
export class LoginService {

    public headers: HttpHeaders;
    private urlApi = urlApi;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
    }

    public register(user): Observable<any> {
        return this.http.post(`${this.urlApi}/signup`, user, { headers: this.headers });
    }

    public login(account_and_password): Observable<any>  {
        return this.http.post(`${this.urlApi}/signin`, account_and_password, { headers: this.headers });
    }
}
