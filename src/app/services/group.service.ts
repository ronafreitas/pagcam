import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';
@Injectable()
export class GroupService {

  private urlApi: string = urlApi;
  private headers: HttpHeaders;
  private token: string;

  constructor(private http: HttpClient){

    this.token = localStorage.getItem('token')

    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.token);
  }

  public AddGroup(data: any): Observable<any> {
    return this.http.post(`${this.urlApi}/group`, data, { headers: this.headers });
  }

  public GetGroups(page?: number): Observable<any> {
    return this.http.get(`${this.urlApi}/group?page=${page}`, { headers: this.headers });
  }

  public GetGroupByUserId(iduser:number, page?: number): Observable<any> {
    return this.http.get(`${this.urlApi}/groups/user/${iduser}?page=${page}`, { headers: this.headers });
  }

  public GetGroupById(idGroup:any): Observable<any> {
    return this.http.get(`${this.urlApi}/group/${idGroup}`, { headers: this.headers });
  }

  public DeleteGroupById(idGroup:number): Observable<any> {
    return this.http.delete(`${this.urlApi}/group/${idGroup}`, { headers: this.headers });
  }

  public EditGroupById(data: any): Observable<any> {
    return this.http.put(`${this.urlApi}/group`, data, { headers: this.headers });
  }

  public GetGroupByName(groupname): Observable<any> {
    return this.http.get(`${this.urlApi}/group/name/${groupname}`, { headers: this.headers });
  }

  // publicações
  public AddPublication(data: any): Observable<any> {
    return this.http.post(`${this.urlApi}/group/publication`, data, { headers: this.headers });
  }
  public GetPublicationsByGroupId(idGroup:any): Observable<any> {
    return this.http.get(`${this.urlApi}/group/publications/${idGroup}`, { headers: this.headers });
  }
  public AddCommentByPublicationIdGroup(data: any): Observable<any> {
    return this.http.post(`${this.urlApi}/group/comment`, data, { headers: this.headers });
  }

}