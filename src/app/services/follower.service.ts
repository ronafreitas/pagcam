import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';

@Injectable()
export class FollowerService {

  private urlApi: string = urlApi;
  private headers: HttpHeaders;
  private token: string;

  constructor(private http: HttpClient){

    this.token = localStorage.getItem('token')
    
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.token);

  }

  public AddFollower(data): Observable<any> {
    return this.http.post(`${this.urlApi}/follow`, data, { headers: this.headers });
  }

  public RemoveFollower(data): Observable<any> {
    return this.http.post(`${this.urlApi}/unfollow`, data, { headers: this.headers });
  }

  public GetFollowerByUserId(userId, page?): Observable<any> {
    return this.http.get(`${this.urlApi}/follower/${userId}?page=${page}`, { headers: this.headers });
  }

  public GetFollowingByUserId(userId, page?): Observable<any> {
    return this.http.get(`${this.urlApi}/following/${userId}?page=${page}`, { headers: this.headers });
  }

  public GetCountFollowingsByUserId(userId): Observable<any> {
    return this.http.get(`${this.urlApi}/followinsgcount/${userId}`, { headers: this.headers });
  }

  public GetCountFollowersByUserId(userId): Observable<any> {
    return this.http.get(`${this.urlApi}/followerscount/${userId}`, { headers: this.headers });
  }

}