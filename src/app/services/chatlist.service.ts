import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlApi } from '../config/index';

@Injectable()
export class ChatlistService {

  private urlApi = urlApi;
  private headers: HttpHeaders;
  private token: string;

  constructor(private http: HttpClient) {

    this.token = localStorage.getItem('token')

    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.token);

  }



  // Observable<T> | any
  public GetChatlistByUserId(page=0, userId): Observable<any> {
    return this.http.get(`${this.urlApi}/chatlist/${userId}?page=${page}`, { headers: this.headers });
  }


  

  public AddChatlist(newchat): Observable<any> {
    return this.http.post(`${this.urlApi}/chatlist`, newchat, { headers: this.headers });
  }

}