import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { PublishComponent } from './pages/publish/publish.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { FollowersComponent } from './pages/followers/followers.component';
import { FollowingComponent } from './pages/following/following.component';
import { ExtractComponent } from './pages/extract/extract.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { CreditsComponent } from './pages/credits/credits.component';
import { CallingComponent } from './pages/calling/calling.component';
import { UserComponent } from './pages/user/user.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';
import { SearchComponent } from './pages/search/search.component';
import { GroupComponent } from './pages/group/group.component';
import { CreateComponent } from './pages/group/create/create.component';
import { EditComponent } from './pages/group/edit/edit.component';
import { ListComponent } from './pages/group/list/list.component';
import { ShowComponent } from './pages/group/show/show.component';
import { PublicationComponent } from './pages/group/publication/publication.component';
const routes: Routes = [
  //{ path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent},
  { path: 'profile', component: ProfileComponent},
  { path: 'publish', component: PublishComponent },
  { path: 'messages', component: MessagesComponent },
  { path: 'followers', component: FollowersComponent },
  { path: 'following', component: FollowingComponent },
  { path: 'extract', component: ExtractComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'credits', component: CreditsComponent },
  { path: 'calling/:id', component: CallingComponent },
  { path: 'user/:id', component: UserComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'search', component: SearchComponent },
  { path: 'edit-profile', component: EditProfileComponent },
  { path: 'groups', component: GroupComponent },
    { path: 'group/:id', component: ShowComponent },
    { path: 'edit-group/:id', component: EditComponent },
    { path: 'publication-group/:id/:name', component: PublicationComponent },
    { path: 'create-group', component: CreateComponent },
    { path: 'list-groups', component: ListComponent },
  { path: '**', component: LoginComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}