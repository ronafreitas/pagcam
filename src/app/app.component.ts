import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  shcom: boolean = true;
  constructor(private router: Router){
    let lus = JSON.parse(localStorage.getItem('l'));
    router.events.forEach((event) => {
      if(event instanceof NavigationStart){
        if(event['url'] == '/login' || event['url'] == '/register'){
          this.shcom = false;
          localStorage.clear();
        }else{
          if(event['url'] == '/'){
            if(lus == null){
              this.shcom = false;
              localStorage.clear();
              this.router.navigate(['/login']);
            }else{
              this.shcom = false;
              this.router.navigate(['/login']);
              localStorage.clear();
            }
          }else{
            if(lus == null){
              this.shcom = false;
              this.router.navigate(["/login"], {replaceUrl:true});
              localStorage.clear();
            }else{
              if(lus == null){
                this.shcom = true;
              }else{
                this.shcom = true;
              }
            }
          }
        }
      }
    });
  }
}