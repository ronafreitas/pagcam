import { Component,OnInit} from '@angular/core';
import { UserService } from '../../services/user.service';
import { SocketService } from '../../services/socket.service';
import { Router, NavigationStart } from '@angular/router';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit{
  countmess:number=0;
  ifcount:boolean=false;
  setsockm:boolean=false;
  user:any=JSON.parse(localStorage.getItem('user')) || {};
  constructor(private router: Router,private userService: UserService,public socketservice: SocketService){}
  ngOnInit(){
    let ts = this;
    this.router.events.forEach((event) => {
      if(event instanceof NavigationStart){
        if(event['url'] == '/messages'){
          ts.setsockm=false;
        }else{
          ts.setsockm=true;
        }
      }
    });
    this.socketservice.onSocket().subscribe(res => {
      if(ts.setsockm){
        ts.ifcount=true;
        this.countmess++;
      }
    });
  }
  resetCnMes(){
    this.ifcount=false;
    this.countmess=0;
  }
  logout(){
    const UserD = {
      online:false
    }
    this.userService.UpdateProfile(this.user._id,UserD).subscribe(
      ret => {
        localStorage.clear();
        window.location.href = '/login';
      },
      err => {
        console.log(err)
      }
    )
  }
}
