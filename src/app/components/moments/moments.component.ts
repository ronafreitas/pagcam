import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
@Component({
  selector: 'app-moments',
  templateUrl: './moments.component.html',
  styleUrls: ['./moments.component.css']
})
export class MomentsComponent implements OnInit {
  
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor(){}

  ngOnInit(){

      this.galleryOptions = [
        {
          "image": false,
          width: '100%',
          height: '100px',
          thumbnailsColumns: 8,
          imageAnimation: NgxGalleryAnimation.Slide
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '100%',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
    ];

    this.galleryImages = [
      
    ];

    for (let index = 0; index < 10; index++) {
      //const element = array[index];
      this.galleryImages.push({
          small: './assets/img/1-small.jpeg',
          medium: './assets/img/1-small.jpeg',
          big: './assets/img/1-small.jpeg'
      });
      this.galleryImages.push({
        small: './assets/img/2-small.jpeg',
        medium: './assets/img/2-small.jpeg',
        big: './assets/img/2-small.jpeg'
      });
      this.galleryImages.push({
        small: './assets/img/3-small.jpeg',
        medium: './assets/img/3-small.jpeg',
        big: './assets/img/3-small.jpeg'
      });
    }
  }

}
