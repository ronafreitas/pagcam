import { Component, Input } from '@angular/core';
//,OnChanges - implements OnChanges
//,Output, EventEmitter
import { LikeService } from '../../services/like.service';
import { CommentService } from '../../services/comment.service';
import { GroupService } from '../../services/group.service';
@Component({
  selector: 'feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent{
  user:any = JSON.parse(localStorage.getItem('user'));
  @Input() objectLIst:any=[];
  @Input() tipo:number;// tipo 1 = feed principal e tipo 2 = publicações dos grupos
  //@Output() objectLIstChange = new EventEmitter<any>();

  clickDynamicArrayLoad: Array<any> = [];
  cntf:number;

  constructor(
		private likeService: LikeService,
		private gs:GroupService,
		private commentService: CommentService,){}

	// <textarea... (keyup)="AddComment($event,userPublication._id,i)"
	t(array){
		this.clickDynamicArrayLoad = array
	}

	// <img.. src="{{p(listPublications[i].like,user._id) ? 'assets/img/..svg' : 'assets/img/svg/..svg'}}"
	p(likeArray){
		const x = likeArray.filter(item => item.userId._id == this.user._id);
		return x.length > 0 ? true : false
	}

	comentar(idpubl:any,totlpubl:number,arrComen:any){
		var vlinp = (<HTMLInputElement>document.getElementById('comtr_'+idpubl));
		vlinp.blur();
		document.getElementById('co_pre_'+idpubl).style.display='block';

		const body = {
			comment: vlinp.value,
			publicationId: idpubl,
			userId: this.user._id,
		}

		if(this.tipo == 1){
			this.commentService.AddComment(body).subscribe(
				res => {
					document.getElementById('co_pre_'+idpubl).style.display='none';
					let newcom = {
						_id: idpubl,
						​​​​​comment: vlinp.value,
						​​​​​creationDate: "",
						​​​​​publicationId: idpubl,
						​​​​​userId: {
							_id: this.user._id,
							​​​​​​avatar: this.user.avatar,
							​​​​​​displayName: this.user.displayName
						}
					}
					if(totlpubl > 0){
						arrComen.push(newcom);
					}else{
						var divcom = (<HTMLInputElement>document.getElementById('comment_'+idpubl));
						divcom.style.display='block';
						arrComen.push(newcom);
					}
					vlinp.value = '';
				},
				err => {
					console.log(err)
				}
			)
		}else if(this.tipo == 2){
			this.gs.AddCommentByPublicationIdGroup(body).subscribe(
				res => {
					document.getElementById('co_pre_'+idpubl).style.display='none';
					let newcom = {
						_id: idpubl,
						​​​​​comment: vlinp.value,
						​​​​​creationDate: "",
						​​​​​publicationId: idpubl,
						​​​​​userId: {
							_id: this.user._id,
							​​​​​​avatar: this.user.avatar,
							​​​​​​displayName: this.user.displayName
						}
					}
					if(totlpubl > 0){
						arrComen.push(newcom);
					}else{
						var divcom = (<HTMLInputElement>document.getElementById('comment_'+idpubl));
						divcom.style.display='block';
						arrComen.push(newcom);
					}
					vlinp.value = '';
				},
				err => {
					console.log(err)
				}
			)
		}
	}

	showcomts(target:any,idpubl:any){
		var divcom = (<HTMLInputElement>document.getElementById('comment_'+idpubl));
		divcom.style.display='block';
		target.style.display='none';
	}

	curtir_descurtir(publicatId:any){
		var vldclass = document.getElementById('heart_'+publicatId);
		// curtir
		if(!vldclass.classList.contains('descurtir_cor')){
			vldclass.classList.remove('curtir_cor');
			vldclass.classList.add('descurtir_cor');

			const body = {
			  publicationId: publicatId,
			  userId: this.user._id,
			}
			var ttcur = document.getElementById('crt_'+publicatId).innerText;
			var psrint = parseInt(ttcur);
			this.cntf = psrint+1;
			var vss = this.cntf.toString();
			document.getElementById('crt_'+publicatId).innerText = vss;

			if(this.tipo == 1){
				this.AddLikeFeed(body);
			}else if(this.tipo == 2){
				this.AddLikeGroup(body);
			}
		// descurtir
		}else{
			const body = {
			  publicationId: publicatId,
			  userId: this.user._id,
			}
			vldclass.classList.remove('descurtir_cor');
			vldclass.classList.add('curtir_cor');
			var ttcur = document.getElementById('crt_'+publicatId).innerText;
			var psrint = parseInt(ttcur);
			if(psrint > 0){
				this.cntf = psrint-1;
			}else{
				this.cntf = 0;
			}
			var vss = this.cntf.toString();
			document.getElementById('crt_'+publicatId).innerText = vss;

			if(this.tipo == 1){
				this.RemoveLikeFeed(body);
			}else if(this.tipo == 2){
				this.RemoveLikeGroup(body);
			}
		}
	}

	public AddLikeFeed(data:any) {
		this.likeService.AddLikeFeed(data).subscribe(
		  res => {
		    //this.socketservice.AddLike(data, pubUserId, this.user);
		  },
		  err => {
		    console.log(err)
		  })
	}
	public AddLikeGroup(data:any) {
		this.likeService.AddLikeGroup(data).subscribe(
		  res => {
		    //this.socketservice.AddLike(data, pubUserId, this.user);
		  },
		  err => {
		    console.log(err)
		  })
	}

	public RemoveLikeFeed(data:any) {
		this.likeService.RemoveLikeFeed(data).subscribe(
		  res => {
		    //this.socketservice.RemoveLike(data, pubUserId, this.user);
		  },
		  err => {
		    console.log(err)
		  })
  }
	public RemoveLikeGroup(data:any) {
		this.likeService.RemoveLikeGroup(data).subscribe(
		  res => {
		    //this.socketservice.RemoveLike(data, pubUserId, this.user);
		  },
		  err => {
		    console.log(err)
		  })
  }

	orderByDate(items){ 
    return items.sort((a: any, b: any) =>
      new Date(a.date).getTime() - new Date(b.date).getTime()
    );
	}
	
  /*updateViews() {
    // Add views
    //this.views++;
    this.objectLIst=[]
    this.objectLIst.push({teste:`hello: `+new Date()});

    // Emit the value:
    this.objectLIstChange.emit(this.objectLIst);
  }*/

	/*public Like_And_Unlike(pubId, pubUserId) {

		let x = document.getElementById(`like_${pubId}`)

		const body = {
		  publicationId: pubId,
		  userId: this.user._id,
		}

		let src = {
		  unlike: 'assets/img/svg/456257.svg',
		  like: 'assets/img/svg/456115.svg'
		}

		if(x.innerHTML.includes(src.unlike)){
		  x.querySelector('img').src = src.like
		  this.AddLike(body, pubUserId)
		} else {
		  x.querySelector('img').src = src.unlike
		  this.RemoveLike(body, pubUserId)
		}
	}*/
  
}
